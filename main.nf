#!/usr/bin/env nextflow
/*
========================================================================================
                          GHRU AMR Pipeline
========================================================================================
*/

// DSL 2
nextflow.enable.dsl=2
version = '1.2'

// setup params
default_params = ParamsParser.default_params()
merged_params = default_params + params

// help and version messages
ParamsUtilities.help_or_version(merged_params, version)

final_params = ParamsParser.check_params(merged_params, version, workflow.projectDir.toString())
include {combine_ariba_summaries} from './lib/processes' addParams(final_params)

// include read polishing functionality
read_polising_params_renames = [
  "read_polishing_adapter_file" : "adapter_file",
  "read_polishing_depth_cutoff" : "depth_cutoff"
]
params_for_read_polishing = ParamsUtilities.rename_params_keys(final_params, read_polising_params_renames)
include { polish_reads } from './lib/modules/read_polishing/workflows' addParams(params_for_read_polishing)

// include ariba functionality
ariba_params_renames = [
  "ariba_database_dir" : "database_dir",
  "ariba_summary_arguments" : "summary_arguments",
] 
params_for_ariba = ParamsUtilities.rename_params_keys(final_params, ariba_params_renames)
include { ariba as ariba_for_acquired } from './lib/modules/ariba/workflows' params(params_for_ariba)
include { ariba as ariba_for_point } from './lib/modules/ariba/workflows' params(params_for_ariba)



workflow {
  //Setup input Channel from Read path
  Channel
      .fromFilePairs( final_params.reads_path )
      .ifEmpty { error "Cannot find any reads matching: ${final_params.reads_path}" }
      .set { reads }

  polished_reads = polish_reads(reads)

  // Run Ariba with ncbi acquired database
  ariba_acquired_summary_output = ariba_for_acquired(polished_reads, file(params_for_ariba.database_dir), params_for_ariba.summary_arguments)
  if (final_params.species){
    pointfinder_db = file("${workflow.projectDir}/ariba_databases/pointfinder_db_2021-02-17/${final_params.species}_db")
    ariba_point_summary_output = ariba_for_point(polished_reads,pointfinder_db, '--cluster_cols assembled --known_variants --col_filter n --row_filter n')
    acquired_and_point_summaries = ariba_acquired_summary_output.concat(ariba_point_summary_output).collect()
    combine_ariba_summaries(acquired_and_point_summaries)
  }

}

workflow.onComplete {
  Messages.complete_message(final_params, workflow, version)
}

workflow.onError {
  Messages.error_message(workflow)
}
