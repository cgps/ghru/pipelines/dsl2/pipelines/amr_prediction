class ParamsUtilities{
    public static void help_or_version(Map params, String version){
        // Show help message
        if (params.help){
            Messages.version_message(version)
            Messages.help_message()
            System.exit(0)
        }

        // Show version number
        if (params.version){
            Messages.version_message(version)
            System.exit(0)
        }
    }

    public static String check_mandatory_parameter(Map params, String parameter_name){
        if ( !params[parameter_name]){
            println "You must specifiy a " + parameter_name
            System.exit(1)
        } else {
            return params[parameter_name]
        }
    }

    public static void check_optional_parameters(Map params, List parameter_names){
        if (parameter_names.collect{name -> params[name]}.every{param_value -> param_value == false}){
            println "You must specifiy at least one of these options: " + parameter_names.join(", ")
            System.exit(1)
        }
    }

    public static String check_parameter_value(String parameter_name, String value, List value_options){
        if (value_options.any{ it == value }){
            return value
        } else {
            println "The value (" + value + ")  supplied for " + parameter_name + " is not valid. It must be one of " + value_options.join(", ")
            System.exit(1)
        }
    }

    public static Map rename_params_keys(Map params_to_rename, Map old_and_new_names) {
        old_and_new_names.each{ old_name, new_name ->
            if (params_to_rename.containsKey(old_name))  {
                params_to_rename.put( new_name, params_to_rename.remove(old_name ) )
            }
        }
        return params_to_rename
    }
}